package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"cs425/mp2/src/labrpc"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

//
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
//
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
}

type LogEntry struct {
	Command interface{}
	Term    int
}

type State int8

const (
	Follower State = iota
	Candidate
	Leader
)

//
// A Go object implementing a single Raft peer.
//
type Raft struct {
	mu    sync.Mutex          // Lock to protect shared access to this peer's state
	peers []*labrpc.ClientEnd // RPC end points of all peers
	me    int                 // this peer's index into peers[]
	dead  int32               // set by Kill()

	// Your data here (2A, 2B).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.
	// You may also need to add other state, as per your implementation.

	currentTerm int
	votedFor    int
	log         []LogEntry
	commitIndex int
	lastApplied int

	state      State
	nextIndex  []int
	matchIndex []int

	timeout    time.Duration
	timer      int
	msgChannel chan ApplyMsg
}

func (rf *Raft) reset_timer() {
	rf.mu.Lock()
	rf.timer += 1
	rf.mu.Unlock()
}

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	// Your code here (2A).
	term = rf.currentTerm
	isleader = rf.state == Leader
	return term, isleader
}

//
// example RequestVote RPC arguments structure.
// field names must start with capital letters!
//
type RequestVoteArgs struct {
	// Your data here (2A, 2B).
	Term         int
	CandidateId  int
	LastLogIndex int
	LastLogTerm  int
}

//
// example RequestVote RPC reply structure.
// field names must start with capital letters!
//
type RequestVoteReply struct {
	// Your data here (2A).
	Term        int
	VoteGranted bool
}

//
// example RequestVote RPC handler.
//
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (2A, 2B).
	// Read the fields in "args",
	// and accordingly assign the values for fields in "reply".
	reply.Term = rf.currentTerm
	if args.Term < rf.currentTerm {
		reply.VoteGranted = false
	} else if args.Term > rf.currentTerm {
		rf.state = Follower
		rf.currentTerm = args.Term
		rf.votedFor = -1
	}
	if rf.votedFor == -1 || rf.votedFor == args.CandidateId {
		// compare which one is more up-to-date
		index := len(rf.log)
		if index == 0 {
			reply.VoteGranted = true
		} else {
			logTerm := rf.log[index-1].Term
			if logTerm < args.LastLogTerm {
				reply.VoteGranted = true
			} else if logTerm > args.LastLogTerm {
				reply.VoteGranted = false
			} else {
				reply.VoteGranted = index <= args.LastLogIndex
			}
		}
	} else {
		reply.VoteGranted = false
	}

	if reply.VoteGranted {
		rf.votedFor = args.CandidateId
		rf.state = Follower
		rf.reset_timer()
		go rf.elect_timeout(rf.timer)
		//fmt.Println(rf.me, "vote for", args.CandidateId)
	}
}

//
// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
//
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	return ok
}

//
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election. even if the Raft instance has been killed,
// this function should return gracefully.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {
	index := -1
	term := -1
	isLeader := true

	// Your code here (2B).
	index = len(rf.log) + 1
	term = rf.currentTerm
	isLeader = rf.state == Leader

	if isLeader {
		rf.mu.Lock()
		rf.log = append(rf.log, LogEntry{command, rf.currentTerm})
		rf.mu.Unlock()
		go rf.send_append_entry()
		//fmt.Println(rf.me, "get", command, index, term)
	}

	return index, term, isLeader
}

//
// the tester doesn't halt goroutines created by Raft after each test,
// but it does call the Kill() method. your code can use killed() to
// check whether Kill() has been called. the use of atomic avoids the
// need for a lock.
//
// the issue is that long-running goroutines use memory and may chew
// up CPU time, perhaps causing later tests to fail and generating
// confusing debug output. any goroutine with a long-running loop
// should call killed() to check whether it should stop.
//
func (rf *Raft) Kill() {
	atomic.StoreInt32(&rf.dead, 1)
	// Your code here, if desired.
}

func (rf *Raft) killed() bool {
	z := atomic.LoadInt32(&rf.dead)
	return z == 1
}

//
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.me = me

	// Your initialization code here (2A, 2B).
	rf.currentTerm = 0
	rf.votedFor = -1
	rf.log = make([]LogEntry, 0)
	rf.commitIndex = -1
	rf.lastApplied = -1
	rf.timeout = time.Duration((rand.Float64()/2 + 0.5) * 1000) // 0.5 to 1.5 seconds
	rf.timer = 0
	rf.msgChannel = applyCh

	go rf.elect_timeout(0)
	go rf.check_apply_log()

	return rf
}

func (rf *Raft) elect_timeout(timer int) {
	time.Sleep(rf.timeout * time.Millisecond)
	if rf.timer <= timer && !rf.killed() {
		rf.elect(rf.currentTerm + 1)
	}
}

func (rf *Raft) elect(term int) {
	rf.currentTerm = term
	rf.state = Candidate
	//fmt.Println(rf.me, "start election, term:", rf.currentTerm)
	lastTerm := 0
	if len(rf.log) > 0 {
		lastTerm = rf.log[len(rf.log)-1].Term
	}

	args := RequestVoteArgs{rf.currentTerm, rf.me, len(rf.log), lastTerm}
	replies := make([]RequestVoteReply, len(rf.peers))
	rf.votedFor = rf.me
	processed := make(chan int)
	for i := 0; i < len(rf.peers); i++ {
		if i != rf.me {
			go func(idx int) {
				rf.sendRequestVote(idx, &args, &replies[idx])
				processed <- idx
			}(i)
		}
	}
	rf.reset_timer()
	go rf.elect_timeout(rf.timer)
	votes := 1
	max_term := rf.currentTerm
	for i := range processed {
		if rf.state != Candidate || term != rf.currentTerm {
			break
		}
		if replies[i].Term > term {
			max_term = replies[i].Term
		}

		if replies[i].VoteGranted {
			votes += 1
		}

		if votes > len(rf.peers)/2 && rf.state == Candidate {
			rf.state = Leader
			go rf.elected(max_term)
			break
		}
	}
}

func (rf *Raft) elected(term int) {
	//fmt.Println(rf.me, "elected, term:", rf.currentTerm)
	rf.reset_timer()
	rf.currentTerm = term
	rf.state = Leader
	rf.matchIndex = make([]int, len(rf.peers))
	rf.nextIndex = make([]int, len(rf.peers))
	index := len(rf.log)
	for i := 0; i < len(rf.peers); i++ {
		rf.nextIndex[i] = index
		rf.matchIndex[i] = -1
	}
	go rf.update_commit()
	rf.send_heartbeat()
}

type AppendEntriesArgs struct {
	Term         int
	LeaderId     int
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []LogEntry
	LeaderCommit int
}
type AppendEntriesReply struct {
	Term    int
	Success bool
}

func (rf *Raft) get_append_args(idx int) AppendEntriesArgs {
	args := AppendEntriesArgs{}
	args.LeaderId = rf.me
	args.Term = rf.currentTerm
	args.LeaderCommit = rf.commitIndex
	if rf.nextIndex[idx] <= 0 {
		args.PrevLogIndex = -1
		args.PrevLogTerm = -1
	} else {
		args.PrevLogIndex = min(rf.nextIndex[idx]-1, len(rf.log)-1)
		args.PrevLogTerm = rf.log[args.PrevLogIndex].Term
	}
	return args
}

func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	rf.reset_timer()
	go rf.elect_timeout(rf.timer)

	if args.Term > rf.currentTerm {
		//fmt.Println(rf.me, "Retire, term:", rf.currentTerm)
		rf.state = Follower
		rf.currentTerm = args.Term
		rf.votedFor = args.LeaderId
	} else if args.Term == rf.currentTerm {
		rf.state = Follower
		rf.votedFor = args.LeaderId
	}
	if args.Term < rf.currentTerm {
		reply.Success = false
	} else {
		if len(rf.log) <= args.PrevLogIndex {
			reply.Success = false
		} else if args.PrevLogIndex >= 0 && rf.log[args.PrevLogIndex].Term != args.PrevLogTerm {
			reply.Success = false
			rf.log = rf.log[:args.PrevLogIndex]
		} else {
			rf.log = append(rf.log[:args.PrevLogIndex+1], args.Entries...)
			reply.Success = true
			if args.LeaderCommit > rf.commitIndex {
				rf.commitIndex = min(args.LeaderCommit, len(rf.log)-1)
			}
		}
	}
	reply.Term = rf.currentTerm
}

func (rf *Raft) send_heartbeat() {
	for {
		if rf.state != Leader || rf.killed() {
			break
		}

		time.Sleep(10 * time.Millisecond)
		//fmt.Println(rf.me, "sending hartbeats, term:", rf.currentTerm)
		rf.send_append_entry()
		time.Sleep((rf.timeout - 200) * time.Millisecond)
	}
}

func (rf *Raft) send_append_entry() {
	for i := 0; i < len(rf.peers); i++ {
		if i == rf.me {
			continue
		}
		go rf.replicate_entry(i)
	}
}

func (rf *Raft) replicate_entry(idx int) {
	if rf.state != Leader {
		return
	}
	args := rf.get_append_args(idx)
	reply := AppendEntriesReply{}
	if args.PrevLogIndex+1 >= len(rf.log) {
		args.Entries = make([]LogEntry, 0)
	} else {
		args.Entries = rf.log[args.PrevLogIndex+1:]
	}
	rf.peers[idx].Call("Raft.AppendEntries", &args, &reply)
	if reply.Term > rf.currentTerm {
		rf.state = Follower
		//fmt.Println(rf.me, "Retire, term:", rf.currentTerm)
		rf.reset_timer()
		rf.elect_timeout(rf.timer)
	} else if reply.Success {
		rf.mu.Lock()
		rf.nextIndex[idx] += len(args.Entries)
		rf.matchIndex[idx] = rf.nextIndex[idx] - 1
		rf.mu.Unlock()
	} else {
		wait := false
		rf.mu.Lock()
		rf.nextIndex[idx] -= 1
		if rf.nextIndex[idx] < 0 {
			wait = true
			rf.nextIndex[idx] = 0
		}
		rf.mu.Unlock()
		if wait {
			time.Sleep(8 * time.Millisecond)
		}
		rf.replicate_entry(idx)
	}
}

func (rf *Raft) update_commit() {
	for {
		time.Sleep(50 * time.Millisecond)
		if rf.state != Leader || rf.killed() {
			break
		}
		minN := rf.commitIndex
		threshold := len(rf.peers) / 2
		pass := 1 // including itself
		for i := 0; i < len(rf.matchIndex); i++ {
			if i == rf.me {
				continue
			}
			if rf.matchIndex[i] > rf.commitIndex {
				pass += 1
				if pass > threshold {
					minN = max(minN, rf.matchIndex[i])
				}
			}
		}
		if minN >= len(rf.log) {
			//fmt.Println(rf.me, "something wrong", rf.currentTerm)
			rf.state = Follower
			rf.reset_timer()
			rf.elect_timeout(rf.timer)
			break
		}
		rf.commitIndex = minN
	}
}

func (rf *Raft) check_apply_log() {
	for {
		time.Sleep(50 * time.Millisecond)
		if rf.killed() {
			break
		}
		if rf.commitIndex > rf.lastApplied {
			commitInex := rf.commitIndex // use a fix value to avoid race condition
			for i := rf.lastApplied + 1; i <= commitInex; i++ {
				rf.apply_log(rf.log[i].Command, i)
			}
			rf.lastApplied = commitInex
		}
	}
}

func (rf *Raft) apply_log(command interface{}, index int) {
	index += 1
	//fmt.Println(rf.me, "Apply ", command, index, rf.currentTerm)
	rf.msgChannel <- ApplyMsg{true, command, index}
}
